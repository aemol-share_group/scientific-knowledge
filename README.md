# 每天更新天气图

### KMA（Korea Meteorological Agency) (2020.10.01-Update)

------

<!--注：时间均为世界协调时UTC-->

#### 网址：https://www.weather.go.kr/w/image/chart/analysis.do

#### 天气分析图介绍:

- ##### 基本分析：

  ###### 1. surface analysis（三小时更新一次）

![sfc3_2020100100](weatherchart_sample/sfc3_2020100100.png)

###### 	2. surface_analysis(十二小时更新一次)

![surf_2020100112](weatherchart_sample/surf_2020100112.png)

###### 	3. 高度场分析（925hPa, 850hPa, 700hPa, 500hPa, 300hPa, 200hPa, 100hPa 每六小时更新一次）

![up85_2020100100](weatherchart_sample/up85_2020100100.png)

- ##### 中等分析:

  ###### 1. 500hPa环流场分析（极射投影）（每六小时更新一次）

  ![kim_n500_anlmod_pb4_2020100100](weatherchart_sample/kim_n500_anlmod_pb4_2020100100.gif)

  ###### 2. 24h 500hPa环流场变化（每六小时更新一次）

  ![kim_n500_difmod_pb4_2020100100](weatherchart_sample/kim_n500_difmod_pb4_2020100100.gif)

  ###### 3.辅助分析图1，2（每六小时更新一次）

  ![kim_gdps_anal_axfe01_pb4_2020100100](weatherchart_sample/kim_gdps_anal_axfe01_pb4_2020100100.gif)

  ![kim_gdps_anal_axfe02_pb4_2020100100](weatherchart_sample/kim_gdps_anal_axfe02_pb4_2020100100.gif)

#### 文件目录结构：(222.195.76.208)

##### `/work/home/jungu/Work/KMA/analysis:`

```
|--- basic
│   |--20201001
|   |--20201002
|   |-- ......

|--- advanced
|   |--20201001
|   |--20201002
|   |-- ......
```



------

### CMA(China Meteorological Agency) (2020.10.04-Update)

#### 网址：http://www.nmc.cn/publish/observations/china/dm/weatherchart-h000.htm

#### 天气分析图介绍：

- ##### 基本天气分析

  ###### 1.地面分析（三小时更新一次）

  ![SEVP_NMC_WESA_SFER_EGH_ACWP_L00_P9_20201004060000000](weatherchart_sample/SEVP_NMC_WESA_SFER_EGH_ACWP_L00_P9_20201004060000000.png)

  <!--站点的填图要素包括：云量、风向风速、天气现象、温度（红色数字）和露点（蓝色数字）；天气系统符号包括：高压中心（H）、低压中心（L）、冷锋（带三角形蓝色线段）、暖锋（带半圆形红色线段）和台风符号；蓝色等值线为海平面气压场等压线（单位hPa）；紫色填充区为六级以上大风速区；绿色线条为等比湿线，其栅格化一侧为比湿达到15g/kg的区域；-->

  ###### 2.高度场分析（925hPa, 850hPa, 700hPa, 500hPa, 200hPa, 100hPa 十二小时更新一次）

  ![SEVP_NMC_WESA_SFER_EGH_ACWP_L85_P9_20201004000000000](weatherchart_sample/SEVP_NMC_WESA_SFER_EGH_ACWP_L85_P9_20201004000000000.png)

<!--站点的填图要素包括：风向风速和温度露点差；天气系统符号包括：高压中心（H）、低压中心（L）、暖中心（W）、冷中心（C）、槽线或切变线（棕色线段）和台风符号；蓝色等值线为等高线（单位dagpm）；红色色等值线为等温线（单位℃），其中零摄氏度以下以虚线显示；紫色填充区为风速达12m/s的急流区；绿色线条为等比湿线，其栅格化一侧为比湿达到12g/kg的区域；-->

- ##### 叠加卫星云图

  ###### 1.地面分析（三小时更新一次）

  ![SEVP_NMC_WESA_SFER_ESPCT_ACWP_L00_P9_20201004060000000](weatherchart_sample/SEVP_NMC_WESA_SFER_ESPCT_ACWP_L00_P9_20201004060000000.png)

  <!--站点的填图要素包括：云量、风向风速、天气现象、温度（红色数字）和露点（蓝色数字）；天气系统符号包括：高压中心（H）、低压中心（L）、冷锋（带三角形蓝色线段）、暖锋（带半圆形红色线段）和台风符号；蓝色等值线为海平面气压场等压线（单位hPa）；底层填色为地形高度；顶层填色为红外卫星云图。-->

  ###### 2.高度场分析（925hPa, 850hPa, 700hPa, 500hPa, 200hPa, 100hPa 十二小时更新一次）

  ![SEVP_NMC_WESA_SFER_ESPCT_ACWP_L85_P9_20201004120000000](weatherchart_sample/SEVP_NMC_WESA_SFER_ESPCT_ACWP_L85_P9_20201004120000000.png)

  <!--站点的填图要素包括：风向风速和温度露点差；天气系统符号包括：高压中心（H）、低压中心（L）、暖中心（W）、冷中心（C）和台风符号；蓝色等值线为等高线（单位dagpm）；红色色等值线为等温线（单位℃），其中零摄氏度以下以虚线显示。底层填色为地形高度；顶层填色为红外卫星云图。-->

- ##### 叠加雷达拼图

  ###### 1.地面分析（三小时更新一次）

  ![SEVP_NMC_WESA_SFER_ESPBT_ACWP_L00_P9_20201004060000000](weatherchart_sample/SEVP_NMC_WESA_SFER_ESPBT_ACWP_L00_P9_20201004060000000.png)

  <!--站点的填图要素包括：云量、风向风速、天气现象、温度（红色数字）和露点（蓝色数字）；天气系统符号包括：高压中心（H）、低压中心（L）、冷锋（带三角形蓝色线段）、暖锋（带半圆形红色线段）和台风符号；蓝色等值线为海平面气压场等压线（单位hPa）；底层填色为地形高度；顶层填色为雷达组合反射率因子拼图。-->

  ###### 2.高度场分析（925hPa, 850hPa, 700hPa, 500hPa, 200hPa, 100hPa 十二小时更新一次）

  ![SEVP_NMC_WESA_SFER_ESPBT_ACWP_L85_P9_20201004000000000](weatherchart_sample/SEVP_NMC_WESA_SFER_ESPBT_ACWP_L85_P9_20201004000000000.png)

  <!--站点的填图要素包括：风向风速和温度露点差；天气系统符号包括：高压中心（H）、低压中心（L）、暖中心（W）、冷中心（C）和台风符号；蓝色等值线为等高线（单位dagpm）；红色色等值线为等温线（单位℃），其中零摄氏度以下以虚线显示。底层填色为地形高度；顶层填色为雷达组合反射率因子拼图。-->
#### 文件目录结构：(222.195.76.208)

##### `/work/home/jungu/Work/CMA/analysis:`

```
|--- Basic
│   |--20201004
|   |--20201005
|   |-- ......

|--- Cloud
|   |--20201004
|   |--20201005
|   |-- ......

|--- Radar
|   |--20201004
|   |--20201005
|   |-- ......
```


